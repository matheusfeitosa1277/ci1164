#include "matrix.h"
#include <string.h>

double *precision; /* Por padrao, MatrixSquare armazena um array de doubles */

MatrixSquare *matrix_square_create(int m) {
    int i;
    MatrixSquare *matrix;

    matrix = malloc(sizeof(MatrixSquare) + m * sizeof(precision));

    matrix->m = m;

    for (i = 0; i < m; ++i) 
        matrix->coefficients[i] = malloc(sizeof(*precision) * m);

    return matrix;
}

void matrix_square_destroy(MatrixSquare *matrix) {
    while (matrix->m)
        free(matrix->coefficients[--matrix->m]);

    free(matrix);
}

/* --- */

char CHAR_BUF[LINESIZE];

void matrix_square_read(MatrixSquare *matrix) { /* Talvez usar strtof */
    char *token;
    int i, j;

    for (i = 0, j = 0; i < matrix->m; ++i, j = 0) {
        fgets(CHAR_BUF, LINESIZE, stdin);    

        token = strtok(CHAR_BUF, " ");

        do 
            matrix->coefficients[i][j++] = atof(token); 
        while (token = strtok(NULL, " "));

    }
}

/* --- */

void matrix_square_print(MatrixSquare *matrix) {
    int i, j;

    printf("%d\n", matrix->m);

    for (i = 0; i < matrix->m; ++i) {
        for (j = 0; j < matrix->m; ++j)
            printf("%6.6g ", matrix->coefficients[i][j]);
        putchar('\n');
    }
}

