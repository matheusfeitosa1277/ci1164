#include "matrix.h"
#include <stdio.h>

extern char CHAR_BUF[LINESIZE];

/* Preciso de uma estrutura dinamica pra armazenar as matrizes... */

int main() {
    int m;
    MatrixSquare *matrix;
	
	#ifdef DEBUG
    		printf("foi\n");
	#endif
    
		
	do { /* Pelo menos uma matriz na entrada */
        	fgets(CHAR_BUF, LINESIZE, stdin);
        	sscanf(CHAR_BUF, "%d\n", &m);

       	 	matrix = matrix_square_create(m);
        	matrix_square_read(matrix); 

        	matrix_square_print(matrix);
        	matrix_square_destroy(matrix);

    	} while (getchar() != EOF);

    return 0;
}

