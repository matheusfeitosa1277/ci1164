#ifndef MATRIX_H
#define MATRIX_H

#define LINESIZE 1024

#include <stdio.h> 
#include <stdlib.h> 

/* Implementar um programa para calcular a matriz inversa de matrizes quadradas 
    usando o Método da Fatoração LU com pivoteamento parcial. */

typedef struct { /* Matrix A_{m, n}, m == n */
    int m;
    double *coefficients[0];
} MatrixSquare;

MatrixSquare *matrix_square_create();

void matrix_square_destroy(MatrixSquare *matrix);

/* --- */

/* Recebe uma sequencia de M streams da stdin, composta de doubles separados por ' '
   Salva esses valores nos coefficientes da matriz */

void matrix_square_read(MatrixSquare *matrix);

/* --- */

/*  Imprime o valor M da matriz. Na proxima linha
    Imprime, em grid MxM, os coefficientes da matriz */

void matrix_square_print(MatrixSquare *matrix);

#endif

