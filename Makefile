CC=gcc
DEBUG=-DDEBUG
CFLAG=-Wall -std=c99 $(DEBUG)
NAME=matriz

all: $(NAME)

$(NAME):main.o matrix.o
	$(CC) $(CFLAG) main.o matrix.o -o $(NAME)

main.o: main.c matrix.h
	$(CC) -c $(CFLAG) main.c
matrix.o: matrix.c matrix.h
	$(CC) -c $(CFLAG) matrix.c

clean: 
	rm -f *.o
purge: clean
	rm -f $(NAME)
